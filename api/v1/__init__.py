# pylint: disable=C0325, W0511
"""NotifyCtrl class declaration"""
import json
import os
import string
import random
from flask import current_app as app, request, session
from flask_restful import Resource, reqparse

class NotifyCtrl(Resource):
    # /v1/api/notify
    """Notify util class"""
    @classmethod
    def get(cls):
        """[GET] reachable service"""
        chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
        rdstring = ''.join(random.choice(chars) for _ in range(30))
        return rdstring
