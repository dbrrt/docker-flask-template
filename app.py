#!/usr/bin/python
# -*- coding: utf-8 -*-
"""app.py : Entry file of the application"""
from flask import Flask
from flask_restful import Api as api
import api.v1 as service
from flask_cors import CORS
# Flask App Instantiation
APP = Flask(__name__)
# Instantiating config from `settings.py` file
APP.config.from_object('settings')
# Enabling CORS
CORS(APP, resources={r"/api/v1/*": {"origins": "*"}})
# API Instantiation
API = api(APP)
# modules
API.add_resource(service.NotifyCtrl, '/api/v1/notify')

@APP.route('/')
def default_route():
    """Default route's purpose is to test that the Flask APP is running"""
    return 'APPLICATION is running. DEBUG = ' + str(APP.config.get('DEBUG_API', None))

# Main runner
if __name__ == '__main__':
    APP.run(threaded=True, host='0.0.0.0', port=APP.config['PORT_API'])
