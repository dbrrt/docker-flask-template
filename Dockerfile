FROM ubuntu:latest
MAINTAINER David Barrat "david.barrat@protonmail.com"
RUN apt-get update -y \
  && apt-get install -y python3-pip python3-dev build-essential \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["app.py"]
EXPOSE 8080
