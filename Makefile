IMAGE-NAME=dbrrt/flask-docker

all:	build run

build:
	docker build -t $(IMAGE-NAME) .

run:
	# PORT MACHINE : PORT CONTAINER
	docker run --rm -ti -p 80:8080 $(IMAGE-NAME)

stop: $(LOGDIR)
	-@docker ps | grep $(IMAGE-NAME) | awk '{ print $$1 }' | xargs docker kill > /dev/null
	-@docker ps -a | grep $(IMAGE-NAME) | awk '{ print $$1 }' | xargs docker rm > /dev/null

dev:
	pip3 install -r requirements.txt --user \
	&& python3 app.py
